package Inheritance_pkg;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Shop_CL_Update_Profile extends ShopCL_Login {
  
  @BeforeTest
  public void beforeTest() {
	  
  }

  @Test(priority = 1)
  public void Go_To_profile() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a")).click();
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[4]/a")).click();
	  Thread.sleep(3000);
  }
  
  @Test(priority = 2)
  public void AllFieldBlank() throws InterruptedException {
	  driver.findElement(By.name("user_data[firstname]")).clear();
	  driver.findElement(By.name("user_data[lastname]")).clear();
	  driver.findElement(By.id("phone")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);
	  
	  String msg = driver.findElement(By.xpath("//*[@id=\"checkBlank1\"]/span")).getText();
	  String emsg = "First name cannot be blank";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
  }
  
  @Test (priority = 3)
  public void FirstnmFilled() throws InterruptedException {
	
	  driver.findElement(By.name("user_data[firstname]")).sendKeys("Nipa");
	  
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);
	  
	  String msg = driver.findElement(By.xpath("//*[@id=\"checkBlank2\"]/span")).getText();
	  String emsg = "Last name cannot be blank";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
	  
  }
  
  @Test (priority = 4)
  public void SecondnmFilled() throws InterruptedException {
	  
	  driver.findElement(By.name("user_data[lastname]")).sendKeys("VoraMehta");
	  
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);
	  
	  String msg = driver.findElement(By.xpath("//*[@id=\"checkBlank3\"]/span")).getText();
	  String emsg = "Mobile number cannot be blank";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
  }
  
  
  @Test (priority = 5)
  public void InvalidmobilenmFilled() throws InterruptedException {
	  
	  driver.findElement(By.id("phone")).sendKeys("00000");
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);
	  
	  String msg = driver.findElement(By.xpath("//*[@id=\"mnumber\"]/span")).getText();
	  String emsg = "Enter correct mobile number";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
  }
  
  
  @Test (priority = 6)
  public void SpCharInFName() throws InterruptedException {
	  
	  driver.findElement(By.name("user_data[firstname]")).clear();
	  driver.findElement(By.id("phone")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.name("user_data[firstname]")).sendKeys("Ni'pa");
	  driver.findElement(By.id("phone")).sendKeys("9000000000");
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);
	  
	  String msg = driver.findElement(By.xpath("//*[@id=\"fname\"]/span")).getText();
	  String emsg = "Enter correct first name";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
  }
  
  
  @Test (priority = 7)
  public void SpCharInLName() throws InterruptedException {
	  
	  driver.findElement(By.name("user_data[firstname]")).clear();
	  driver.findElement(By.name("user_data[lastname]")).clear();
	   Thread.sleep(3000);
	  
	  driver.findElement(By.name("user_data[firstname]")).sendKeys("Nipey");
	  driver.findElement(By.name("user_data[lastname]")).sendKeys("Vora'Mehta");
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);
	  
	  String msg = driver.findElement(By.xpath("//*[@id=\"lname\"]/span")).getText();
	  String emsg = "Enter correct last name";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
  }
  
  
  @Test (priority = 8)
  public void AlphabetsInMoNo() throws InterruptedException {
	  
	  driver.findElement(By.name("user_data[lastname]")).clear();
	  driver.findElement(By.id("phone")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.name("user_data[lastname]")).sendKeys("VoraMehta");
	  driver.findElement(By.id("phone")).sendKeys("abcdefghij");
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);
	  
	  String msg = driver.findElement(By.xpath("//*[@id=\"mnumber\"]/span")).getText();
	  String emsg = "Enter correct mobile number";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
  }
  
  @Test (priority = 9)
  public void UpdatedData() throws InterruptedException {
	  
	   driver.findElement(By.id("phone")).clear();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.id("phone")).sendKeys("7984431355");
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);
	  
	  String msg = driver.findElement(By.xpath("//*[@id=\"notification_85214fdf7d646b0d64eeb251383996d0\"]/div/div[2]")).getText();
	  String emsg = "Profile has been updated successfully.";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
	  driver.navigate().back();
  }
  
  
  
  @AfterTest
  public void afterTest() {
  }

}
