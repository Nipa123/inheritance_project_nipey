package Inheritance_pkg;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Sh_C_Add_To_Cart extends Change_PW {
  
  @BeforeTest
  public void beforeTest() {
  }

  @Test(priority = 19)
  public void ScrollDown() throws InterruptedException {
	  JavascriptExecutor js = (JavascriptExecutor)driver;
	  js.executeScript("scrollBy(0, 600)");
	  Thread.sleep(5000);
  }
  
  @Test(priority =20)
  public void Select_Product() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"127779866\"]/img")).click();
	  Thread.sleep(5000);
	 
	  ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	  driver.switchTo().window(tabs.get(1)); 
	  Thread.sleep(5000);
	
	  String name = driver.findElement(By.xpath("//*[@id=\"main_data\"]/div[2]/div[2]/h1")).getText();
	  String  ename = "KSJ USB Optical Wired Mouse Black";
	  Assert.assertEquals(name, ename);
	  Thread.sleep(5000);
	  
	  JavascriptExecutor js1 = (JavascriptExecutor)driver;
	  js1.executeScript("scrollBy(0, 500)");
	  Thread.sleep(5000);
	  
	  driver.findElement(By.xpath("//*[@id=\"add_cart\"]")).click();
	  
	  Thread.sleep(5000);
	  
  }
  
  @Test(priority = 21)
  public void View_Cart_PopUp() throws InterruptedException {
	  
	  Actions action = new Actions(driver);
	  WebElement element = driver.findElement(By.xpath("/html/body/div[3]/div/div/div[4]/ul/li[4]/a/span"));
	  action.moveToElement(element).perform();
	  Thread.sleep(3000);
	  
	  String name = driver.findElement(By.xpath("/html/body/div[3]/div/div/div[4]/ul/li[4]/div/div/div[2]/ul/li/a/div/span[1]")).getText();
	  String ename = "KSJ USB Optical Wired Mouse Black";
	  Assert.assertEquals(name, ename);
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("/html/body/div[3]/div/div/div[4]/ul/li[4]/div/div/div[3]/a[1]")).click();
	  Thread.sleep(5000);
  }
  
  @Test(priority = 22)
  public void View_Cart() throws InterruptedException {
	  
	  String msg = driver.findElement(By.xpath("//*[@id=\"cart-main-content\"]/div/div[1]/h2")).getText();
	  String emsg = "My Cart ( 1 Item )";
	  Assert.assertEquals(msg,emsg);
	  Thread.sleep(3000);
	  
	  String name = driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[1]/h5/a")).getText();
	  String ename = "KSJ USB Optical Wired Mouse Black";
	  Assert.assertEquals(name, ename);
	  Thread.sleep(3000);
	  
	  String price = driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[4]/span[1]")).getText();
	  String eprice = "Rs 245";
	  Assert.assertEquals(price, eprice);
	  Thread.sleep(3000);
	  
	  String Totprice = driver.findElement(By.xpath("//*[@id=\"gt-cart-price\"]/p[1]/span")).getText();
	  String eTotprice = "Rs 245";
	  Assert.assertEquals(Totprice, eTotprice);
	  Thread.sleep(3000);
	  
	  String gprice = driver.findElement(By.xpath("//*[@id=\"gt-cart-price\"]/p[2]/span")).getText();
	  String egprice = "Rs 245";
	  Assert.assertEquals(gprice, egprice);
	  Thread.sleep(3000);
  }
  
  @Test (priority =23)
  public void PlusInItem() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[2]/span/a[2]")).click();
	  Thread.sleep(3000);
	  
	  String price = driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[4]/span[1]")).getText();
	  String eprice = "Rs 491";
	  Assert.assertEquals(price, eprice);
	  Thread.sleep(3000);
	  
	  String Totprice = driver.findElement(By.xpath("//*[@id=\"gt-cart-price\"]/p[1]/span")).getText();
	  String eTotprice = "Rs 491";
	  Assert.assertEquals(Totprice, eTotprice);
	  Thread.sleep(3000);
	  
	  String gprice = driver.findElement(By.xpath("//*[@id=\"gt-cart-price\"]/p[2]/span")).getText();
	  String egprice = "Rs 491";
	  Assert.assertEquals(gprice, egprice);
	  Thread.sleep(3000);
  }
  
  @Test (priority = 24)
  public void MinusInItem() throws InterruptedException {
	  driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[2]/span/a[1]")).click();
	  Thread.sleep(3000);
	  
	  String price = driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[4]/span[1]")).getText();
	  String eprice = "Rs 245";
	  Assert.assertEquals(price, eprice);
	  Thread.sleep(3000);
	  
	  String Totprice = driver.findElement(By.xpath("//*[@id=\"gt-cart-price\"]/p[1]/span")).getText();
	  String eTotprice = "Rs 245";
	  Assert.assertEquals(Totprice, eTotprice);
	  Thread.sleep(3000);
	  
	  String gprice = driver.findElement(By.xpath("//*[@id=\"gt-cart-price\"]/p[2]/span")).getText();
	  String egprice = "Rs 245";
	  Assert.assertEquals(gprice, egprice);
	  Thread.sleep(3000);
  }
  
  @Test (priority = 25)
  public void RemoveItem() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[2]/a")).click();
	  Thread.sleep(3000);
	  
	  String msg = driver.findElement(By.xpath("//*[@id=\"loadContent\"]/div[2]/div[2]/h3/span")).getText();
	  String emsg ="Remove/Move to Wishlist";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
	  String nmsg = driver.findElement(By.xpath("//*[@id=\"loadContent\"]/div[2]/div[2]/div[1]/div[2]/div[1]")).getText();
	  String enmsg ="KSJ USB Optical Wired Mouse Bl...";
	  Assert.assertEquals(nmsg, enmsg);
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("//*[@id=\"loadContent\"]/div[2]/div[2]/div[2]/a[1]")).click();
	  Thread.sleep(3000);
 
  }
  
  @Test(priority = 26)
  public void WindowClose() throws InterruptedException {
	  driver.close();
	  Thread.sleep(5000);
	  ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
	  driver.switchTo().window(tab.get(0));
	 
	  Thread.sleep(5000);
  }
  
  @AfterTest
  public void afterTest() {
  }
}