package Inheritance_pkg;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Change_PW extends Shop_CL_Update_Profile {
	 @BeforeTest
	  public void beforeTest() {
	  }
	
 @Test(priority = 10)
  public void OpenChangePW() throws InterruptedException {

	  driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[5]/div/div/div[2]/div/div/div/div[1]/ul/li[4]/a")).click();
	  Thread.sleep(3000);
	  
  } 
  
  @Test(priority = 11)
  public void AlPWBlank() throws InterruptedException {
	 
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);          
	 
	  String msg = driver.findElement(By.xpath("//*[@id=\"passwordBlank\"]/span")).getText();
	  String emsg = "Current password cannot be empty";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
	  
  }
  
  @Test(priority = 12)
  public void CurrentPWBlank() throws InterruptedException {
	  
	  driver.findElement(By.id("password1")).sendKeys("12345678");
	  Thread.sleep(1000);
	  driver.findElement(By.id("password2")).sendKeys("12345678");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);          
	 
	  String msg = driver.findElement(By.xpath("//*[@id=\"passwordBlank\"]/span")).getText();
	  String emsg = "Current password cannot be empty";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
	  driver.navigate().refresh();
	  
  }
  
  @Test(priority = 13)
  public void NewPWBlank() throws InterruptedException {
	  
	  driver.findElement(By.id("passwordc")).sendKeys("nemisuriji");
	  Thread.sleep(1000);
	  driver.findElement(By.id("password2")).sendKeys("12345678");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);          
	 
	  String msg = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]/span")).getText();
	  String emsg = "New password cannot be empty";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
	  driver.navigate().refresh();
	  Thread.sleep(3000);
  }
  
  @Test(priority = 14)
  public void ConformPWBlank() throws InterruptedException {
	  
	  driver.findElement(By.id("passwordc")).sendKeys("nemisuriji");
	  Thread.sleep(1000);
	  driver.findElement(By.id("password1")).sendKeys("nipa1234");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);          
	 
	  String msg = driver.findElement(By.xpath("//*[@id=\"password2Blank\"]/span")).getText();
	  String emsg = "Confirm password cannot be empty";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
	  driver.navigate().refresh();
	  Thread.sleep(3000);
  }
  
  @Test(priority = 15)
  public void OnlyNumericPW() throws InterruptedException {
	  driver.findElement(By.id("passwordc")).sendKeys("nemisuriji");
	  Thread.sleep(1000);
	  driver.findElement(By.id("password1")).sendKeys("12345678");
	  Thread.sleep(1000);
	  driver.findElement(By.id("password2")).sendKeys("12345678");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);          
	 
	  String msg = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
	  String emsg = "Password must be alphanumeric";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
	  driver.navigate().refresh();
	  Thread.sleep(3000);
  }
  
  @Test(priority = 16)
  public void OnlyAlphabeticPW() throws InterruptedException {
	  driver.findElement(By.id("passwordc")).sendKeys("nemisuriji");
	  Thread.sleep(1000);
	  driver.findElement(By.id("password1")).sendKeys("nipamehta");
	  Thread.sleep(1000);
	  driver.findElement(By.id("password2")).sendKeys("nipamehta");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);          
	 
	  String msg = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
	  String emsg = "Password must be alphanumeric";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
	  driver.navigate().refresh();
	  Thread.sleep(3000);
  }
  
  @Test(priority = 17)
  public void DiffPW() throws InterruptedException {
	  driver.findElement(By.id("passwordc")).sendKeys("nemisuriji");
	  Thread.sleep(1000);
	  driver.findElement(By.id("password1")).sendKeys("nipa1234");
	  Thread.sleep(1000);
	  driver.findElement(By.id("password2")).sendKeys("1234nipa");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);          
	 
	  String msg = driver.findElement(By.xpath("//*[@id=\"newEqualToConfirm\"]")).getText();
	  String emsg = "New password and confirm new password do not match";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
	  driver.navigate().refresh();
	  Thread.sleep(3000);
  }
  
  @Test(priority = 18)
  public void CaseSensitivityPW() throws InterruptedException {
	  driver.findElement(By.id("passwordc")).sendKeys("nemisuriji");
	  Thread.sleep(1000);
	  driver.findElement(By.id("password1")).sendKeys("nipa1234");
	  Thread.sleep(1000);
	  driver.findElement(By.id("password2")).sendKeys("NIPA1234");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(3000);          
	 
	  String msg = driver.findElement(By.xpath("//*[@id=\"newEqualToConfirm\"]")).getText();
	  String emsg = "New password and confirm new password do not match";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
	  
	  driver.navigate().refresh();
	  Thread.sleep(3000);
  }
  @Test(priority = 18)
  public void Clear_AllPW() throws InterruptedException {
	  driver.findElement(By.id("passwordc")).clear();
	  Thread.sleep(1000);
	  driver.findElement(By.id("password1")).clear();
	  Thread.sleep(1000);
	  driver.findElement(By.id("password2")).clear();
	  Thread.sleep(3000);
	  
	  driver.navigate().back();
	  Thread.sleep(5000);
	  driver.navigate().back();
}
  
 

  @AfterTest
  public void afterTest() {
  }

}
